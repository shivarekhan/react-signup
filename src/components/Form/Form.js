import React from "react";
import validator from "validator";
import isEmail from "validator/lib/isEmail";
import "./Form.css";

class Form extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            firstName: "",
            lastName: "",
            age: 0,
            gender: "",
            role: "",
            email: "",
            password: "",
            reEnterPassword: "",
            Error: {},
            success: "",
            firstNameValid: false,
            lastNameValid: false,
            ageValid: false,
            isGenderSelected: false,
            isRoleSelected: false,
            emailValid: false,
            passwordValid: false,
            rePasswordValid: false,
            isPasswordMatched: false,
            isChecked: false,
            isSubmitted: false,
            anyError: "",
        }
    }

    firstNameValidation = (event) => {
        if (validator.isAlpha(event.target.value) === true) {
            console.log(event.target.value)
            this.setState(
                {
                    firstName: event.target.value,
                    firstNameValid: true
                })
            this.setState((prevState) => {
                { prevState.Error.firstName = "" }
            })
        } else {
            this.setState({ firstName: "", firstNameValid: false })
            this.setState((prevState) => {
                { prevState.Error.firstName = "*Please Enter Valid FirstName" }
            })
        }

    }


    lastNameValidation = (event) => {
        if (validator.isAlpha(event.target.value) === true) {
            console.log(event.target.value)
            this.setState(
                {
                    lastName: event.target.value,
                    lastNameValid: true
                })
            this.setState((prevState) => {
                { prevState.Error.lastName = "" }
            })
        } else {
            this.setState({ lastName: "", lastNameValid: false })
            this.setState((prevState) => {
                { prevState.Error.lastName = "*Please Enter Valid lastName" }
            })
        }

    }

    ageValidation = (event) => {
        if (validator.isInt(event.target.value, { min: 0, max: 100 }) === true) {
            this.setState({ age: event.target.value, ageValid: true })
            this.setState((prevState) => {
                { prevState.Error.age = "" }
            })
        } else {
            this.setState({ age: event.target.value, ageValid: false })
            this.setState((prevState) => {
                { prevState.Error.age = "*Age Must be in Numbers b/w 0 to 100" }
            })
        }
    }

    genderValidation = (event) => {
        console.log(event.target.value)
        if (event.target.value !== "") {
            this.setState({
                gender: event.target.value,
                isGenderSelected: true,
            })
            this.setState((prevState) => {
                { prevState.Error.gender = "" }
            })

        } else {
            this.setState(
                {
                    gender: event.target.value,
                    isGenderSelected: false
                })
            this.setState((prevState) => {
                { prevState.Error.gender = "*Select valid gender from the given list" }
            })
        }
    }

    roleValidation = (event) => {
        if (event.target.value !== "") {
            this.setState({
                role: event.target.value,
                isRoleSelected: true,
            })
            this.setState((prevState) => {
                { prevState.Error.role = "" }
            })
        } else {
            this.setState({
                role: event.target.value,
                isRoleSelected: true,
            })
            this.setState((prevState) => {
                { prevState.Error.role = "*Select the valid role from the given choices" }
            })
        }
    }

    emailValidation = (event) => {
        if (isEmail(event.target.value)) {
            this.setState({
                email: event.target.value,
                emailValid: true
            })
            this.setState((prevState) => {
                { prevState.Error.email = "" }
            })
        } else {
            this.setState({
                email: event.target.value,
                emailValid: false
            })
            this.setState((prevState) => {
                { prevState.Error.email = "Please enter a valid email in format username@domain.com/.in/.org" }
            })

        }
    }

    passwordValidation = (event) => {
        if (validator.isStrongPassword(event.target.value) === true) {
            this.setState(
                {
                    password: event.target.value,
                    passwordValid: true
                })
            this.setState((prevState) => {
                { prevState.Error.password = "" }
            })
        } else {
            this.setState({ password: event.target.value, passwordValid: false })
            this.setState((prevState) => {
                { prevState.Error.password = "*Password must contain 1 uppercase,1 lowerCase,1 numeric,1 special char, min 8 chars" }
            })
        }



    }

    reEnterPasswordValidation = (event) => {
        const { password } = this.state
        if (validator.isStrongPassword(event.target.value) === true) {
            this.setState({ reEnterPassword: event.target.value, rePasswordValid: true })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "" }
            })
        } else {
            this.setState({ reEnterPassword: event.target.value, rePasswordValid: false })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "*Password must contain 1 uppercase,1 lowerCase,1 numeric,1 special char, min 8 chars" }
            })
        }

        if (password !== event.target.value) {
            this.setState({ isPasswordMatched: false })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "*Password must be equal in both entries" }
            })
        } else {
            this.setState({ isPasswordMatched: true })
            this.setState((prevState) => {
                { prevState.Error.reEnterPassword = "" }
            })
        }

    }

    checkBoxValidation = (event) => {
        const { isChecked } = this.state
        if (isChecked === false) {
            this.setState({ isChecked: true })
            this.setState((prevState) => {
                { prevState.Error.isChecked = "" }
            })
        } else {
            this.setState({ isChecked: false })
            this.setState((prevState) => {
                { prevState.Error.isChecked = "please accept terms and condition" }
            })
        }



    }

    validateForm = (event) => {
        event.preventDefault();
        const { firstNameValid,
            lastNameValid,
            ageValid,
            isGenderSelected,
            isRoleSelected,
            emailValid,
            passwordValid,
            rePasswordValid,
            isPasswordMatched,
            isChecked, anyError } = this.state;

        if (firstNameValid && lastNameValid &&
            ageValid &&
            isGenderSelected &&
            isRoleSelected &&
            emailValid &&
            passwordValid &&
            rePasswordValid &&
            isPasswordMatched &&
            isChecked) {
            console.log("ok")
            this.setState(
                {
                    Success: "SuccessFully Registered Thank you",
                    isSubmitted: true
                })
            this.setState((prevState) => {
                { prevState.Error.isChecked = "" }
            })

        } else {
            this.setState({
                anyError: "fill all details carefully",
                isSubmitted: false,

            })



        }
    }

    render() {

        const { isSubmitted, Error } = this.state;
        console.log(Error)
        if (isSubmitted) {
            return <h1>Signup Success</h1>
        } else {
            return (
                <div className="form-container">
                    <div className="form-left-section">
                        <div className='left-cont'>
                            <div className='heading-left-portion'>
                                <h1>Welcome!</h1>
                            </div>
                            <div className='content-left-portion'>
                                <span>
                                    To keep connected with us please Sign in with your personal info.
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="form-right-section">
                        <form onSubmit={this.validateForm}>
                            <div className="form-group">
                                <h1>{this.state.anyError}</h1>
                                <label htmlFor="firstName">First Name</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="firstName"
                                    placeholder="Enter First Name"
                                    name="firstName"
                                    onBlur={this.firstNameValidation}
                                />
                                <small id="firstName" name="firstName" className="form-text">
                                    {Error["firstName"]}
                                </small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="lastName">Last Name</label>
                                <input
                                    type="text"
                                    className="form-control border-red"
                                    id="lastName"
                                    placeholder="Enter Last Name"
                                    name="lastName"
                                    onBlur={this.lastNameValidation}
                                />
                                <small id="lastName" name="lastName" className="form-text">
                                    {Error["lastName"]}
                                </small>
                            </div>

                            <div className="form-group">
                                <label htmlFor="age">Age</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="age"
                                    placeholder="Enter age"
                                    name="age"
                                    onBlur={this.ageValidation}
                                />
                                <small id="age" name="age" className="form-text">
                                    {Error["age"]}
                                </small>
                            </div>
                            <div className="scroll-down-container">
                                <div className="form-group gender">
                                    <label htmlFor="gender-select">Gender</label>
                                    <select
                                        className="form-control"
                                        id="gender-select"
                                        placeholder="select gender"
                                        name="gender"
                                        onBlur={this.genderValidation}
                                    >
                                        <option value="">Select your gender</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Other">Other</option>
                                    </select>
                                    <small id="gender" name="gender" className="form-text">
                                        {Error["gender"]}
                                    </small>
                                </div>

                                <div className="form-group role">
                                    <label htmlFor="role-select">Role</label>
                                    <select
                                        className="form-control"
                                        id="role-select"
                                        name="role"
                                        onBlur={this.roleValidation}
                                    >
                                        <option value="">select your role</option>
                                        <option value="Developer">Developer</option>
                                        <option value="Senior Developer">Senior Developer</option>
                                        <option value="Lead Engineer">Lead Engineer</option>
                                        <option value="CTO">CTO</option>
                                    </select>
                                    <small id="role" name="role" className="form-text">
                                        {Error["role"]}
                                    </small>
                                </div>
                            </div>

                            <div className="form-group">
                                <label htmlFor="exampleFormControlInput1">Email address</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="exampleFormControlInput1"
                                    placeholder="name@example.com"
                                    name="email"
                                    onBlur={this.emailValidation}
                                />
                                <small id="email" name="email" className="form-text">
                                    {Error["email"]}
                                </small>
                            </div>

                            <div className="password-container">
                                <div className="form-group password">
                                    <label htmlFor="exampleInputPassword1">Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="exampleInputPassword1"
                                        placeholder="Password"
                                        name="password"
                                        onBlur={this.passwordValidation}
                                    />
                                    <small id="password" name="password" className="form-text">
                                        {Error["password"]}
                                    </small>
                                </div>

                                <div className="form-group repeat-password">
                                    <label htmlFor="repeat-password">Repeat Password</label>
                                    <input
                                        type="password"
                                        className="form-control"
                                        id="repeat-password"
                                        placeholder="Repeat Password"
                                        name="reEnterPassword"
                                        onBlur={this.reEnterPasswordValidation}
                                    />
                                    <small id="repeat-password" name="reEnterPassword" className="form-text">
                                        {Error["reEnterPassword"]}
                                    </small>
                                </div>
                            </div>
                            <div className="check-box-container">
                                <input
                                    type="checkbox"
                                    id="check-box-id"
                                    name="checkBox"
                                    onClick={this.checkBoxValidation}
                                />
                                <label htmlFor="check-box-id"> I agree to the terms and conditions.</label>
                            </div>
                            <button className="btn-right" type="submit">Sign Up</button>
                        </form>
                    </div>
                </div>
            )
        }
    }
}


export default Form;